package com.skyliner.user.domain;

public enum UserRole {

	ROLE_ADMIN,
	ROLE_USER
}

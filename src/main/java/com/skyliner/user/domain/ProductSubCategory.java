package com.skyliner.user.domain;

public enum ProductSubCategory {
	
	SHIRT,
	TSHIRT,
	SHOES,
	PAINT,
	SAREE,
	KURTA,
	WATCH

}

package com.skyliner.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.skyliner.modal.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

}

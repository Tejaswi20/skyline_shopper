package com.skyliner.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.skyliner.modal.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}

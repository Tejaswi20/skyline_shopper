package com.skyliner.service;

import java.util.List;

import com.skyliner.exception.UserException;
import com.skyliner.modal.User;

public interface UserService {
	
	public User findUserById(Long userId) throws UserException;
	
	public User findUserProfileByJwt(String jwt) throws UserException;
	
	public List<User> findAllUsers();

}

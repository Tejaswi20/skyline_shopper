package com.skyliner.service;

import com.skyliner.modal.OrderItem;

public interface OrderItemService {
	
	public OrderItem createOrderItem(OrderItem orderItem);

}

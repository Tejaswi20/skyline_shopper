package com.skyliner.service;

import com.skyliner.exception.CartItemException;
import com.skyliner.exception.UserException;
import com.skyliner.modal.Cart;
import com.skyliner.modal.CartItem;
import com.skyliner.modal.Product;

public interface CartItemService {
	
	public CartItem createCartItem(CartItem cartItem);
	
	public CartItem updateCartItem(Long userId, Long id,CartItem cartItem) throws CartItemException, UserException;
	
	public CartItem isCartItemExist(Cart cart,Product product,String size, Long userId);
	
	public void removeCartItem(Long userId,Long cartItemId) throws CartItemException, UserException;
	
	public CartItem findCartItemById(Long cartItemId) throws CartItemException;
	
}

package com.skyliner.service;

import com.skyliner.exception.ProductException;
import com.skyliner.modal.Cart;
import com.skyliner.modal.CartItem;
import com.skyliner.modal.User;
import com.skyliner.request.AddItemRequest;

public interface CartService {
	
	public Cart createCart(User user);
	
	public CartItem addCartItem(Long userId,AddItemRequest req) throws ProductException;
	
	public Cart findUserCart(Long userId);

}
